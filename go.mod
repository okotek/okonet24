module okonet

go 1.20

require okotek.ai/x/okocfg v1.1.1

require (
	github.com/korylprince/ipnetgen v1.0.1
	okotek.ai/x/okofrm v1.1.1
)

require (
	gocv.io/x/gocv v0.35.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace okotek.ai/x/okocfg => ../okocfg

replace okotek.ai/x/okofrm => ../okofrm
