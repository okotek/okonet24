package okonet

import (
	"encoding/json"
	"fmt"
	"net"
	"sync"

	"github.com/korylprince/ipnetgen"

	"okotek.ai/x/okocfg"
)

var GLOBAL_NEIGHBORHOOD Neighborhood

type Neighborhood struct {
	Me        Neighbor            //Describes the process this is part of
	Neighbors map[string]Neighbor //Other computers on the lan. "map[ip address as string]full neighbor encoded data"
}

type Neighbor struct {
	Id  uint64
	IP4 net.IP
	IP6 net.IPAddr

	//If storing about self or with neighbor who doesn't have a conn yet, conn is nil.
	ConMap map[string]net.Conn //Multiple net.Conn to a single IPv4. String is executable names that the Neighbor offers, describing what processes in oko24 are running on that address
}

// Iterate over all possible IPs in the subnet, try to connect and share Neighbor handshake.
func searchLan(meName string) error {
	//meName is the name of the process callg the function

	//Get master config stuff
	mast := okocfg.GetDatPuller()

	subnet := mast(meName)["subnet"]
	//procId := mast("procId")
	meData := mast(meName)["meData"]

	if gen, err := ipnetgen.New("192.168.0.0" + subnet); err != nil {
		//handle failure like a man
		panic("Failed to get network subnet iterator:" + err.Error())
	} else {
		var wg sync.WaitGroup
		for ipTmp := gen.Next(); ipTmp != nil; ipTmp = gen.Next() {

			wg.Add(1)
			go func(ip net.IP) {

				var neig Neighbor

				//Run the handshake process
				if con, err := net.Dial("tcp", ip.String()+":7791"); err != nil {

					//FAILED TO HANDSHAKE THIS ADDRESS
					fmt.Println("Error in test dial")
				} else {

					//Send data abou ourself to that neighbor
					enc := json.NewEncoder(con)
					if encErr := enc.Encode(meData); encErr != nil {
						panic("SEARCHLAN PANIC: " + encErr.Error())
					}

					if decErr := json.NewDecoder(con).Decode(&neig); decErr != nil {
						//Handle decode error:
						panic("FAILED IN DECODE HANDSHAKE " + decErr.Error())
					} else {
						//Put this neighbor in the master list
						GLOBAL_NEIGHBORHOOD.Neighbors[ip.String()] = neig
					}

					wg.Done()
				}

			}(ipTmp)
		}

		wg.Wait()
	}

	return nil //TODO: actually handle the errors

}
