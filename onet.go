package okonet

import (
	okofrm "okotek.ai/x/okofrm"

	"encoding/gob"
	"fmt"
	"net"
)

func Sendoff(input chan okofrm.Frame, targ string, killSwitch *bool) {
	conn, conErr := net.Dial("tcp", targ)
	if conErr != nil {
		fmt.Println("Error in sendoff: ", conErr)
	}

	//maintainCon(&conn)

	enc := gob.NewEncoder(conn)

	for {
		fmt.Printf("\r sendoff loop ----")
		select {
		case frm := <-input:

			if err := enc.Encode(&frm); err != nil {
				fmt.Println("Failure on encode: ", err)
			}

		default:
			fmt.Printf("\r default loop")
			if *killSwitch {
				break
			}

		}
	}
}

// Listen for incoming connections, convert to Frame, pipe them out.
func Pickup(output chan okofrm.Frame, lnTarg string, killSwitch *bool) {

	if lsn, lnErr := net.Listen("tcp", lnTarg); lnErr != nil {
		fmt.Println("Failed to listen: ", lnErr)
	} else {

		var tmpDec okofrm.Frame
		for {

			if conn, conErr := lsn.Accept(); conErr != nil {
				fmt.Println("Conn fail: ", conErr)
			} else {

				go func() {
					for {
						if *killSwitch {
							lsn.Close()
							return
						}
					}
				}()

				dec := gob.NewDecoder(conn)
				if decerr := dec.Decode(&tmpDec); decerr != nil {
					fmt.Println("Failed to get decode: ", decerr)
				} else {
					if *killSwitch {
						return
					}
					output <- tmpDec
				}
			}

		}
	}
}
