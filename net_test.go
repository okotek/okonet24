package okonet

import (
	"fmt"
	"testing"

	okofrm "okotek.ai/x/okofrm"
)

func TestNet(t *testing.T) {

	fmt.Println("Starting testnet")

	dumpchan := make(chan okofrm.Frame)
	sendchan := make(chan okofrm.Frame)
	var killSwitch bool = false

	fmt.Println("Starting net funcs")
	go Pickup(dumpchan, ":8080", &killSwitch)
	go Sendoff(sendchan, "localhost:8080", &killSwitch)

	fmt.Println("Starting sendoff")
	tFrm := okofrm.Frame{}
	sendchan <- tFrm

	fmt.Println("Sent; waiting")
	got := <-dumpchan

	fmt.Println("I got", got.Met.Chans, "channels, but a ditch aint one")
	killSwitch = true
	return

}

func TestLenna(t *testing.T) {

	fmt.Println("Starting lenna test")
	dumpChan := make(chan okofrm.Frame)
	sendChan := make(chan okofrm.Frame)
	var killSwitch bool = false

	go Pickup(dumpChan, ":8080", &killSwitch)
	go Sendoff(sendChan, "localhost:8080", &killSwitch)

	tFrm, _ := okofrm.BuildFromImg("lenna.png", "asdf", "asdf")
	sendChan <- tFrm
	got := <-dumpChan

	fmt.Println("GOT: ", got)
	killSwitch = true
}
